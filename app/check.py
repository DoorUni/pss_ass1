import sys

def read_file(file):
	with open(file) as f:
		lines = f.readlines()

	cleared_file = []
	for l in lines:
		cleared_file.append(l.rstrip('\n'))

	return cleared_file


output_file = read_file(sys.argv[1])
check_file = read_file(sys.argv[2])

for il, cl in zip(output_file, check_file):
	assert il == cl