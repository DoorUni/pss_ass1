FROM gcc:4.9
COPY . /app
WORKDIR /app
RUN apt-get update && apt-get install -y build-essential && apt-get install -y libxml2 && apt-get install -y bison && apt-get install -y gengetopt && apt-get install -y make
RUN cd app && cd igraph-0.7.1 && ./configure && make -j4 && make install && ldconfig
RUN cd app && make
#RUN cd app && ./main.exe -f test.txt -t e -o output.txt
#ENTRYPOINT ["./app/main.exe"]